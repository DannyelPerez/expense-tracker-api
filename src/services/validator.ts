import {HttpErrors} from '@loopback/rest';
import isemail from 'isemail';
import {Credentials} from '../repositories/user.repository';

const passwordRegex = /(?=^.{8,15}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
export function validateCredentials(credentials: Credentials) {
  if (isemail.validate(credentials.email, {errorLevel: true}) > 0) {
    throw new HttpErrors.UnprocessableEntity('invalid email');
  }

  if (
    !credentials.password ||
    credentials.password.length < 8 ||
    !passwordRegex.test(credentials.password)
  ) {
    throw new HttpErrors.UnprocessableEntity(
      'Passwords must have at least 8 characters and contain at least one of each of the following: uppercase letters, lowercase letters, numbers, and symbols.',
    );
  }
}
