import {authenticate, TokenService} from '@loopback/authentication';
import {
  MyUserService,
  TokenServiceBindings,
  UserServiceBindings,
} from '@loopback/authentication-jwt';
import {inject} from '@loopback/core';
import {property, repository} from '@loopback/repository';
import {
  getModelSchemaRef,
  HttpErrors,
  post,
  Request,
  requestBody,
  RestBindings,
} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {genSalt, hash} from 'bcryptjs';
import _ from 'lodash';
import {User} from '../models';
import {
  AccessTokenRepository,
  Credentials,
  UserRepository,
} from '../repositories';
import {validateCredentials} from '../services';

class NewUserRequest extends User {
  @property({
    type: 'string',
    required: true,
  })
  password: string;
}

const CredentialsSchema = {
  type: 'object',
  required: ['email', 'password'],
  properties: {
    email: {
      type: 'string',
      format: 'email',
    },
    password: {
      type: 'string',
      minLength: 8,
    },
  },
};

const CredentialsRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {schema: CredentialsSchema},
  },
};
export class UserController {
  constructor(
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public jwtService: TokenService,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: MyUserService,
    @inject(SecurityBindings.USER, {optional: true})
    public user: UserProfile,
    @repository(UserRepository) protected userRepository: UserRepository,
    @repository(AccessTokenRepository)
    protected accessTokenRepository: AccessTokenRepository,
  ) {}

  @post('/signup', {
    responses: {
      '200': {
        description: 'User',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': User,
            },
          },
        },
      },
    },
  })
  async signUp(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(NewUserRequest, {
            title: 'NewUser',
          }),
        },
      },
    })
    newUserRequest: NewUserRequest,
  ): Promise<User> {
    validateCredentials(_.pick(newUserRequest, ['email', 'password']));
    const userExist = await this.userRepository.find({
      where: {email: {eq: newUserRequest.email}},
    });
    if (userExist && userExist.length) {
      throw new HttpErrors.Conflict('Email value is already taken');
    }
    try {
      const password = await hash(newUserRequest.password, await genSalt());
      const savedUser = await this.userRepository.create(
        _.omit(newUserRequest, 'password'),
      );
      await this.userRepository
        .userCredentials(savedUser.id)
        .create({password});
      return savedUser;
    } catch (error) {
      throw error;
    }
  }

  @post('/users/login', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })
  async login(
    @requestBody(CredentialsRequestBody) credentials: Credentials,
  ): Promise<{token: string; userId: string}> {
    validateCredentials(_.pick(credentials, ['email', 'password']));
    const user = await this.userService.verifyCredentials(credentials);
    const userProfile = this.userService.convertToUserProfile(user);
    const token = await this.jwtService.generateToken(userProfile);
    await this.accessTokenRepository.create({token});
    return {token, userId: userProfile[securityId]};
  }

  @authenticate('sessionAuth')
  @post('/users/logout', {
    responses: {
      '200': {
        description: 'Logout success',
      },
    },
  })
  async logout(
    @inject(RestBindings.Http.REQUEST)
    request: Request,
  ): Promise<void> {
    if (!request.headers.authorization) {
      throw new HttpErrors.Unauthorized(`Authorization header not found.`);
    }
    const authHeaderValue = request.headers.authorization;
    const token = authHeaderValue.split(' ')[1];
    this.accessTokenRepository.deleteAll({token: {eq: token}});
  }
}
