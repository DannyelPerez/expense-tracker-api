import {authenticate} from '@loopback/authentication';
import {repository} from '@loopback/repository';
import {get, getModelSchemaRef} from '@loopback/rest';
import {Category, RecordType} from '../models';
import {CategoryRepository, RecordTypeRepository} from '../repositories';
@authenticate('sessionAuth')
export class OptionsController {
  constructor(
    @repository(CategoryRepository)
    protected categoryRepository: CategoryRepository,
    @repository(RecordTypeRepository)
    protected recordTypeRepository: RecordTypeRepository,
  ) {}

  @get('/options/categories', {
    responses: {
      '200': {
        description: 'Categories model instance',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Category),
            },
          },
        },
      },
    },
  })
  async getCategories(): Promise<Category[]> {
    return this.categoryRepository.find();
  }

  @get('/options/recordTypes', {
    responses: {
      '200': {
        description: 'Record types model instance',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(RecordType),
            },
          },
        },
      },
    },
  })
  async getRecordsTypes(): Promise<RecordType[]> {
    return this.recordTypeRepository.find();
  }
}
