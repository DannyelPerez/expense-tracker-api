import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Account,
  Record,
} from '../models';
import {AccountRepository} from '../repositories';

export class AccountRecordController {
  constructor(
    @repository(AccountRepository) protected accountRepository: AccountRepository,
  ) { }

  @get('/accounts/{id}/records', {
    responses: {
      '200': {
        description: 'Array of Account has many Record',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Record)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Record>,
  ): Promise<Record[]> {
    return this.accountRepository.records(id).find(filter);
  }

  @post('/accounts/{id}/records', {
    responses: {
      '200': {
        description: 'Account model instance',
        content: {'application/json': {schema: getModelSchemaRef(Record)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Account.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Record, {
            title: 'NewRecordInAccount',
            exclude: ['id'],
            optional: ['accountId']
          }),
        },
      },
    }) record: Omit<Record, 'id'>,
  ): Promise<Record> {
    return this.accountRepository.records(id).create(record);
  }

  @patch('/accounts/{id}/records', {
    responses: {
      '200': {
        description: 'Account.Record PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Record, {partial: true}),
        },
      },
    })
    record: Partial<Record>,
    @param.query.object('where', getWhereSchemaFor(Record)) where?: Where<Record>,
  ): Promise<Count> {
    return this.accountRepository.records(id).patch(record, where);
  }

  @del('/accounts/{id}/records', {
    responses: {
      '200': {
        description: 'Account.Record DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Record)) where?: Where<Record>,
  ): Promise<Count> {
    return this.accountRepository.records(id).delete(where);
  }
}
