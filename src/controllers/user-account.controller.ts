import {authenticate} from '@loopback/authentication';
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {Account, User} from '../models';
import {UserRepository} from '../repositories';

@authenticate('sessionAuth')
export class UserAccountController {
  constructor(
    @repository(UserRepository) protected userRepository: UserRepository,
  ) {}

  @get('/users/{id}/accounts', {
    responses: {
      '200': {
        description: 'Array of User has many Account',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Account)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Account>,
  ): Promise<Account[]> {
    return this.userRepository.accounts(id).find(filter);
  }

  @post('/users/{id}/accounts', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {'application/json': {schema: getModelSchemaRef(Account)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof User.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Account, {
            title: 'NewAccountInUser',
            exclude: ['id'],
            optional: ['userId'],
          }),
        },
      },
    })
    account: Omit<Account, 'id'>,
  ): Promise<Account> {
    return this.userRepository.accounts(id).create(account);
  }

  @patch('/users/{userId}/accounts/{accountId}', {
    responses: {
      '200': {
        description: 'User.Account PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('userId') userId: number,
    @param.path.number('accountId') accountId: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Account, {partial: true}),
        },
      },
    })
    account: Partial<Account>,
  ): Promise<Account | {}> {
    await this.userRepository.accounts(userId).patch(account, {id: accountId});
    const updatedAccount = await this.userRepository
      .accounts(userId)
      .find({where: {id: {eq: accountId}}});
    return updatedAccount.length ? updatedAccount[0] : {};
  }

  @del('/users/{id}/accounts', {
    responses: {
      '200': {
        description: 'User.Account DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Account))
    where?: Where<Account>,
  ): Promise<Count> {
    return this.userRepository.accounts(id).delete(where);
  }
}
