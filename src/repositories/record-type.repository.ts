import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {RecordType, RecordTypeRelations, Record} from '../models';
import {DbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {RecordRepository} from './record.repository';

export class RecordTypeRepository extends DefaultCrudRepository<
  RecordType,
  typeof RecordType.prototype.id,
  RecordTypeRelations
> {

  public readonly records: HasManyRepositoryFactory<Record, typeof RecordType.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('RecordRepository') protected recordRepositoryGetter: Getter<RecordRepository>,
  ) {
    super(RecordType, dataSource);
    this.records = this.createHasManyRepositoryFactoryFor('records', recordRepositoryGetter,);
  }
}
