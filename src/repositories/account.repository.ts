import {Getter, inject} from '@loopback/core';
import {
  DefaultCrudRepository,
  HasManyRepositoryFactory,
  repository,
} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Account, AccountRelations, Record} from '../models';
import {RecordRepository} from './record.repository';

export class AccountRepository extends DefaultCrudRepository<
  Account,
  typeof Account.prototype.id,
  AccountRelations
> {
  public readonly records: HasManyRepositoryFactory<
    Record,
    typeof Account.prototype.id
  >;
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('RecordRepository')
    protected recordRepositoryGetter: Getter<RecordRepository>,
  ) {
    super(Account, dataSource);
    this.records = this.createHasManyRepositoryFactoryFor(
      'records',
      recordRepositoryGetter,
    );
    this.registerInclusionResolver('records', this.records.inclusionResolver);
  }
}
