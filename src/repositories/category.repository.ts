import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {Category, CategoryRelations, Record} from '../models';
import {DbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {RecordRepository} from './record.repository';

export class CategoryRepository extends DefaultCrudRepository<
  Category,
  typeof Category.prototype.id,
  CategoryRelations
> {

  public readonly records: HasManyRepositoryFactory<Record, typeof Category.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('RecordRepository') protected recordRepositoryGetter: Getter<RecordRepository>,
  ) {
    super(Category, dataSource);
    this.records = this.createHasManyRepositoryFactoryFor('records', recordRepositoryGetter,);
  }
}
