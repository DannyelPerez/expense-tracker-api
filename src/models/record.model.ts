import {Entity, model, property} from '@loopback/repository';

@model()
export class Record extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  accountId: number;

  @property({
    type: 'number',
    required: true,
  })
  recordTypeId: number;

  @property({
    type: 'number',
    required: true,
  })
  categoryId: number;

  @property({
    type: 'number',
    required: true,
  })
  value: number;

  @property({
    type: 'number',
  })
  balance?: number;

  @property({
    type: 'date',
    required: true,
  })
  date: string;

  constructor(data?: Partial<Record>) {
    super(data);
  }
}

export interface RecordRelations {
  // describe navigational properties here
}

export type RecordWithRelations = Record & RecordRelations;
