import {Entity, model, property, hasMany} from '@loopback/repository';
import {Record} from './record.model';

@model()
export class RecordType extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  description: string;

  @hasMany(() => Record)
  records: Record[];

  constructor(data?: Partial<RecordType>) {
    super(data);
  }
}

export interface RecordTypeRelations {
  // describe navigational properties here
}

export type RecordTypeWithRelations = RecordType & RecordTypeRelations;
