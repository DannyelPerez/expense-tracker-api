import {
  AuthenticateFn,
  AuthenticationBindings,
  AUTHENTICATION_STRATEGY_NOT_FOUND,
  USER_PROFILE_NOT_FOUND,
} from '@loopback/authentication';
import {Context, inject} from '@loopback/context';
import {
  DefaultSequence,
  FindRoute,
  InvokeMethod,
  ParseParams,
  Reject,
  RequestContext,
  RestBindings,
  Send,
} from '@loopback/rest';

const SequenceActions = RestBindings.SequenceActions;
export class MySequence extends DefaultSequence {
  @inject(AuthenticationBindings.AUTH_ACTION)
  protected authenticateRequest: AuthenticateFn;

  constructor(
    @inject(RestBindings.Http.CONTEXT) public ctx: Context,
    @inject(SequenceActions.FIND_ROUTE) protected findRoute: FindRoute,
    @inject(SequenceActions.PARSE_PARAMS) protected parseParams: ParseParams,
    @inject(SequenceActions.INVOKE_METHOD) protected invoke: InvokeMethod,
    @inject(SequenceActions.SEND) public send: Send,
    @inject(SequenceActions.REJECT) public reject: Reject,
  ) {
    super(findRoute, parseParams, invoke, send, reject);
  }

  async handle(context: RequestContext) {
    try {
      const {request} = context;
      await this.authenticateRequest(request);
      super.handle(context);
    } catch (err) {
      if (
        err.code === AUTHENTICATION_STRATEGY_NOT_FOUND ||
        err.code === USER_PROFILE_NOT_FOUND
      ) {
        Object.assign(err, {statusCode: 401 /* Unauthorized */});
      }
      this.reject(context, err);
    }
  }
}
